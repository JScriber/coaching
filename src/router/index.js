import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from '@/views/global/Home';
import About from '@/views/global/About';
import Login from '@/views/global/Login';

import DayForm from '@/views/client/DayForm';
import History from '@/views/client/History';

import ListClient from '@/views/coach/ListClient';
import CreateClient from '@/views/coach/CreateClient';
import FollowClient from '@/views/coach/FollowClient';

import store from '@/store';

Vue.use(VueRouter)

const routes = [
  // Global.
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },

  // Coaching.
  {
    path: '/client',
    name: 'ListClient',
    component: ListClient,
    meta: {
      allowedTo: ['coach'],
    }
  },
  {
    path: '/client/new',
    name: 'CreateClient',
    component: CreateClient,
    meta: {
      allowedTo: ['coach'],
    }
  },
  {
    path: '/client/:id',
    name: 'FollowClient',
    component: FollowClient,
    meta: {
      allowedTo: ['coach'],
    }
  },

  // Client space.
  {
    path: '/days',
    name: 'History',
    component: History,
    meta: {
      allowedTo: ['client'],
    }
  },
  {
    path: '/days/:id',
    name: 'DayEdit',
    component: DayForm,
    meta: {
      allowedTo: ['client'],
    }
  },
  {
    path: '/days/new',
    name: 'DayNew',
    component: DayForm,
    meta: {
      allowedTo: ['client'],
    }
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  let role;
  
  if (store.getters.connected) {
    role = store.getters.user.role;
  }

  const isAllowed = to.matched.some(route => {
    return route.meta.allowedTo === undefined ||
           route.meta.allowedTo.includes(role);
  });

  if (isAllowed) {
    next();
  } else {
    next({ name: 'Login' });
  }
});

export default router;

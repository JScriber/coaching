import * as axios from 'axios';

/**
 * Coaching API.
 */
export default {

  /**
   * Tries to login the user.
   * @param {*} username 
   * @param {*} password 
   * @returns the user or null if error.
   */
  async login(username, password) {
    const params = { username, password };
    let user = null;

    try {
      const request = await axios.get(`${process.env.VUE_APP_API_URL}/users`, { params });
      const users = request.data;

      if (users.length === 1) {
        user = users[0];
      }
    } catch (e) {
      console.error('Request failed during login.');
    }

    return user;
  },

  /**
   * Lists all the clients.
   * @returns a list of clients.
   */
  async getClients() {
    let clients = [];

    try {
      const params = { role: 'client' };

      const request = await axios.get(`${process.env.VUE_APP_API_URL}/users`, { params });
      clients = request.data;
    } catch (e) {
      console.error('Request failed to get clients.');
    }

    return clients;
  },

  /**
   * Get a client by his ID.
   * @param {number} id 
   */
  async getClient(id) {
    let user = null;

    try {
      const request = await axios.get(`${process.env.VUE_APP_API_URL}/users/${id}`);
      user = request.data;
    } catch (e) {
      console.error('Request failed while fetch client.');
    }

    return user;
  },

  /**
   * Deletes a client by his ID.
   * @param {number} id 
   * @returns true or false
   */
  async deleteClient(id) {
    let deleted;

    try {
      const request = await axios.delete(`${process.env.VUE_APP_API_URL}/users/${id}`);

      deleted = request.data;
    } catch (e) {
      console.error('Failed to delete the client');
      deleted = false;
    }

    return deleted;
  },

  /**
   * Creates a new client.
   * @param {string} username 
   * @param {string} password 
   */
  async createClient(username, password) {
    let success = false;

    try {
      const payload = { username, password, role: 'client' };
      const request = await axios.post(`${process.env.VUE_APP_API_URL}/users`, payload);

      success = request.data;
    } catch (e) {
      console.error('Failed to create the client');
      success = false;
    }

    return success;
  },

  /**
   * Create a report.
   * @param {number} userId 
   * @param {*} report 
   */
  async createReport(userId, report) {
    let success = false;
    report.userId = userId;

    try {
      const request = await axios.post(`${process.env.VUE_APP_API_URL}/days`, report);

      success = request.data !== undefined;
    } catch (e) {
      console.error('Failed to create the report');
      success = false;
    }

    return success;
  },

  /**
   * Update a report.
   * @param {number} id - Report id. 
   * @param {*} report 
   */
  async updateReport(id, report) {
    let success = false;
    report.id = id;

    try {
      const request = await axios.put(`${process.env.VUE_APP_API_URL}/days/${id}`, report);

      success = request.data !== undefined;
    } catch (e) {
      console.error('Failed to update the report');
      success = false;
    }

    return success;
  },

  /**
   * All reports of specific user.
   * @param {number} userId 
   */
  async getReports(userId) {
    let reports = [];

    try {
      const params = { userId };

      const request = await axios.get(`${process.env.VUE_APP_API_URL}/days`, { params });
      reports = request.data;
    } catch (e) {
      console.error('Request failed to get reports of client.');
    }

    return reports;
  },

  /**
   * Get on report by its ID.
   * @param {number} id 
   */
  async getReport(id) {
    let report = null;

    try {
      const params = { id };
      const request = await axios.get(`${process.env.VUE_APP_API_URL}/days`, { params });

      if (request.data.length === 1) {
        report = request.data[0];
      }
    } catch (e) {
      console.error('Request failed to get reports of client.');
    }

    return report;
  },

  /**
   * Sends a message to a user.
   * @param {number} receiverId 
   * @param {number} sender 
   * @param {string} message 
   */
  async sendMessage(receiverId, sender, message) {
    let success = false;

    try {
      const payload = { receiverId, sender, message };
      const request = await axios.post(`${process.env.VUE_APP_API_URL}/messages`, payload);

      success = request.data !== undefined;
    } catch (e) {
      console.error('Failed to send the message.');
      success = false;
    }

    return success;
  },

  /**
   * Get the messages.
   * @param {number} id 
   */
  async getMessages(id) {
    let messages = [];

    try {
      const params = { receiverId: id };

      const request = await axios.get(`${process.env.VUE_APP_API_URL}/messages`, { params });
      messages = request.data;
    } catch (e) {
      console.error('Request failed to get messages of client.');
    }

    return messages;
  }

}


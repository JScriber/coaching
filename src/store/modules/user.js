export default {
  state: {
    user: undefined
  },
  getters: {
    user: state => state.user,
    connected: state => state.user !== undefined,

    isClient: state => state.user && state.user.role === 'client',
    isCoach: state => state.user && state.user.role === 'coach',
  },
  mutations: {
    login(state, user) {
      state.user = user;
    },
    logout(state) {
      state.user = undefined;
    },
  },
  actions: {
    login({commit}, payload) {
      commit('login', payload);
    },
    logout({commit}) {
      commit('logout');
    }
  }
};

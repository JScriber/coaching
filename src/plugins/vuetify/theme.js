import colors from 'vuetify/lib/util/colors'

export default {
  primary: {
    base: colors.red.base,
    darken1: colors.red.darken3,
  },
  secondary: colors.amber,
}
